+++
title = "Canon CR3"
+++

CR3 is the successor of [CR2](../cr2) used by Canon.

The container is a MPEG-4 ISO container instead of the TIFF container
used in CR2.

TBD

# Links

* [Document Canon CR3](https://github.com/lclevy/canon_cr3) through a
  reverse engineering effort. The basis of libopenraw decoding.


