+++
title = "Panasonic RAW and RW2"
+++

Panasonic cameras create RAW files with the RAW or RW2 extension. RW2
is the latest.

Leica rebadged camera also generate RWL files that don't seem to be
different from RW2.

# MIME Type

image/x-panasonic-raw and image/x-panasonic-rw2


# Organisation

TIFF but with a different version in the magic:
`IIU\x00\x08\x00\x00\x00`. Also the tags are uncommon. The CFA is in the
mainIFD that also reference an Exif IFD.

Old RAW files don't have a preview. Newer one (some RAW and all RW2)
have a complete JPEG with Exif embedded.

The RAW data is compressed (proprietary). The CFA bayer layout is
BGGR.


# Links

* http://thinkfat.blogspot.com/2009/02/dissecting-panasonic-rw2-files.html
