+++
title = "Digital camera RAW file formats"
+++

# Supported

The file format libopenraw supports even partially:

* Adobe DNG
* Canon
  * [CRW](crw)
  * [CR2](cr2)
  * [CR3](cr3)
* [Epson ERF](erf)
* [Fuji RAF](raf)
* [Minolta MRW](mrw)
* [Nikon NEF](nef) and NRW
* [Olympus ORF](orf)
* [Panasonic RAW](rw2) and RW2
* [Pentax PEF](pef)
* Sony ARW and [SRF](srf)

## Compression

Known compression schemes used.

* [Lossless JPEG](ljpeg)

# Not Supported

The file format that are not supported due to lack of documentation,
sample or resources. There is a plan to support them in the long
term. Any help is welcome.

* [Foveon X3F](x3f)
* Kodak
  * [DCR](dcr)
  * KDC
* Mamiya MEF
* Sony
  * [SR2](sr2)

# Metadata

## Lens Types

MakerNotes usually store the lens type in some way. Sometime as a
string, sometime as an numerical ID, or something else. There is no
standard and it is mostly per vendor.

### Nikon

See [NEF](nef) documentation

### Pentax

See [PEF](pef) documentation

### Canon

See [CR2](cr2) documentation

# Other references

* [http://crousseau.free.fr/imgfmt_raw.htm](https://web.archive.org/web/20090214101740/http://crousseau.free.fr:80/imgfmt_raw.htm)
  (via the Internet Archive) (in French, by one of the developers of
  DxO)
* The excellent [File Format](http://fileformats.archiveteam.org/wiki/Cameras_and_Digital_Image_Sensors) section from the archive team wiki

# Sample Files

The internet is somewhat rich in sample files. The manufacturers
however don't provide any. Some sources:

* https://raw.pixls.us/ the most
  comprehensive repository for CC-0 RAW files..
* [http://raw.fotosite.pl/](https://web.archive.org/web/20100430021720/http://raw.fotosite.pl:80/)
  (via the Internet Archive) has a public repository of RAW files from
  various camera.
* [http://digikam3rdparty.free.fr/TEST_IMAGES/RAW/](https://web.archive.org/web/20110814152933/http://digikam3rdparty.free.fr/TEST_IMAGES/RAW/)
  from the Digikam team.
* [Imaging Resource](http://www.imaging-resource.com/) has a lot of
  sample files attached to their reviews.
* [DPReview](https://dpreview.com) also has RAW files for comparison
* [Photographyblog](https://www.photographyblog.com/reviews/) has RAW sample
  in ther reviews.
