+++
title = "Foveon X3F"
+++

Format introduced by Sigma SD9 and SD10 digital camera. The Sigma DP1
uses it with some variations.

Official spec:
[http://www.photofo.com/downloads/x3f-raw-format.pdf](https://web.archive.org/web/20110531132056/http://www.photofo.com:80/downloads/x3f-raw-format.pdf) (via the Internet Archive) -
[local copy](x3f-raw-format.pdf)

But the RAW data is "encrypted". dcraw has code in GPL but no documentation per see.  [http://www.photofo.com/x3f-raw-format/] (https://web.archive.org/web/20131126184040/http://www.photofo.com/x3f-raw-format/) (via the Internet Archive) is a project to try to document this.

* [X3F
  Foveon](https://web.archive.org/web/20070807204903/http://www.x3f.info:80/spp/v2_1/english.html),
  the Foveon partial documentation (via the Internet archive -
  2007-08-07).
