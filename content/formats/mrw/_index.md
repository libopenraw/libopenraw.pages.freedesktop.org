+++
title = "Minolta MRW"
+++

Minolta MRW is the format used by Minolta cameras.

* [Dalibor Jelinek's documentation](https://web.archive.org/web/20120724023127/http://www.dalibor.cz:80/minolta/raw_file_format.htm),
  Minolta MRW documentation (via the Internet archive - 2012-07-24)
* [ImageCooker](https://web.archive.org/web/20170123084355/http://www.focalplane.net:80/imagecooker/) (via Internet Archive)
* [Stephane Chauveau
  documentation](https://web.archive.org/web/20060313201319/http://www.chauveau-central.net:80/mrw-format/mrw-format.txt) (via Internet Archive)
* [Cedric Rousseau
  documentation](https://web.archive.org/web/20090214101740/http://crousseau.free.fr:80/imgfmt_raw.htm)
  (via the Internet Archive) from one of the developers of DxO

