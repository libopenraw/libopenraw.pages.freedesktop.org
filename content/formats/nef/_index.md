+++
title = "Nikon NEF and NRW"
+++

NEF files are generated by Nikon cameras.

NRW is a variant, generated by some Coolpix like the P6000 and
P7000. Some differences notable in the RAW data storage that is not
"standard". The use of .nrw extension is probably to differentiate
them since Nikon committed "support" in Windows (like a marketing
gimmick).


# MIME type

image/x-nikon-nef

# Organisation

It is a TIFF file. close to TIFF/EP or DNG.

# Thumbnails

NEF files have a large JPEG preview. It is either in a subIF of the
main IFD (type == thumbail) or for the older, in the MakerNote IFD.

# Compression

D1 files are not compressed.

D1x files are not compressed but have a weird shape: they have a pixel
ratio of 0.5, ie 2 row for one.

Some files are packed (Compression = 32769). It is a standard packing
with some padding: if col % 10 == 9, then an extra 8 bits must be
skipped.

Compression (Compression = 34713) is a Huffman tree and a quantization
table. The quantization tables are at 0x8c and 0x96 tag from the
MakerNote. Strangely dcraw seems to only use the later
as it override the value reading the tags in sequence. See
nikon_compressed_load_raw().

https://blog.majid.info/is-the-nikon-d70-nef-raw-format-truly-lossless/
has a very insightful article about NEF decoding on the D70.

The D100 has a bug that tag uncompressed image as compressed, but not
always. See nikon_is_compressed() in dcraw.c. It appear that most D100
will just use uncompressed (packed) as the compression _froze the
camera for 20s_

NRW are not compressed, but the RAW data organisation is not
standard. (TBD)

# Metadata

## Lens type

The metadata contain the lens type. There is a list:

* [Nikkor Lens
  ID](https://web.archive.org/web/20180403065846/http://www.rottmerhusen.com:80/objektives/lensid/nikkor.html)
* [Nikkor Extended Lens
  ID](https://web.archive.org/web/20171006230418/http://www.rottmerhusen.com/objektives/lensid/thirdparty.html)
