+++
title = "Lossless JPEG"
+++

# Lossless JPEG

Lossless JPEG aka _ljpeg_ is an uncommon JPEG compression method that
is lossless. It is used by [Canon CR2](../cr2), [Kodak DCR](../dcr)
and Adobe DNG

It is currently implemented in libopenraw in
[ljpegdecompressor.cpp](https://gitlab.freedesktop.org/libopenraw/libopenraw/blob/master/lib/ljpegdecompressor.cpp)

## Alternative implementations:

* ljpeg seems to be the only C reference implementation, and, according
  to the JPEG-FAQ, has bugs with 16-bits. This is the code that was
  used as a based for libopenraw implementation.
* Adobe DNG SDK seems to use an adaptation of the former, in C++. Too
  bad the license isn't GPL compatible.
* [jrawio](https://github.com/tidalwave-it/jrawio-src) in Java has an
  implementation that seems to work.
* dcraw, RawSpeed and libraw also implements it.
