+++
title = "Fujifilm RAF"
+++

RAF is the RAW file format used by FujiFilm camera. It
looks like a simple table with embedded JPEG some data and
the raw data.

# MIME type

image/x-fuji-raf

# Organisation

Byte order is Motorola (Big Endian)

* 16 bytes string to identify the file (magic)
  * `FUJIFILMCCD-RAW `
* 4 bytes
  * `0201`
* 8 bytes
  * `FF389501`
* 32 bytes for the camera string, \0 terminated
* offset directory
  * Version (4 bytes) for the directory
    * `0100`
    * `0159`
  * 20 bytes "unknown"
  * Jpeg image offset (4 bytes)
  * Jpeg Image length (4 bytes)
  * Meta container Offset (4 bytes)
  * Meta container Length (4 bytes)
  * CFA Offset (4 bytes)
  * CFA Length (4 bytes)
  * (Jpeg image offset must be greater) 12 bytes unknown.
  * (Jpeg image offset must be greater) Some offset (4 bytes)
* Jpeg image offset
  * Exif JFIF with thumbnail + preview
  * An XMP packet may also be found. This is used by camera to
    indicate favourited image by adding a `Rating` property.
* Meta container offset - Big Endian
  * 4 bytes: count of records
  * Records, one after the other
    * 2 bytes: tag ID
    * 2 bytes: size of record (N)
    * N bytes: data
* CFA Offset
  * TIFF container.
  * RAW data. Maybe compressed.

# Tags

The following tags are in the meta container:

- 0x100: Sensor Dimensions
- 0x110: Active area Top Left
- 0x111: Active area Height Width
- 0x121: Output Height Width
- 0x130: Raw info
- 0x131: CFA pattern: for X-Trans
- 0x2ff0: Camera multiplier (older files)
    - 4x u16, in order: 1, 0, 3, 2. Repeated.
- 0xc000: Other data (?)

# Compression

Recent X-series camera like X-Pro2, X-T3 add an optional compression.

[LibRaw compressed file
support](https://www.libraw.org/news/libraw-compressed-fujifilm-raf-support)

# Links

* [http://crousseau.free.fr/imgfmt_raw.htm](https://web.archive.org/web/20090214101740/http://crousseau.free.fr:80/imgfmt_raw.htm) (via Internet Archive)
