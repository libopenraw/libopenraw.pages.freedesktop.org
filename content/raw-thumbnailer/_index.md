+++
title = "Raw Thumbnailer"
+++

Raw Thumbnailer is a thumbnailer for camera raw files that follow the
thumbnailing spec and thus works in GNOME file manager among
others. It is released under the GPL.

It uses libopnraw in the backend.

# Known limitations

Not enough format are supported.

# Download

* 47.0.1: [.tar.xz](https://download.gnome.org/sources/raw-thumbnailer/47/raw-thumbnailer-47.0.1.tar.xz)

## Legacy

* 3.0.0: [.tar.bz2](https://libopenraw.freedesktop.org/download/raw-thumbnailer-3.0.0.tar.bz2) - [GPG signature](https://libopenraw.freedesktop.org/download/raw-thumbnailer-3.0.0.tar.bz2.asc)
* 0.99.1: [.tar.gz](https://libopenraw.freedesktop.org/download/raw-thumbnailer-0.99.1.tar.gz) - [GPG signature](https://libopenraw.freedesktop.org/download/raw-thumbnailer-0.99.1.tar.gz.asc)
* 0.99: [.tar.gz](https://libopenraw.freedesktop.org/download/raw-thumbnailer-0.99.tar.gz) - [GPG signature](https://libopenraw.freedesktop.org/download/raw-thumbnailer-0.99.tar.gz.asc)

# Source code

The repository is at
https://gitlab.gnome.org/World/gnome-raw-thumbnailer
