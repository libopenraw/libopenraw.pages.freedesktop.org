Start by [getting the code](../getting-the-code).


# Dependencies

* a C++ compiler with the standard library
* boost
* libjpeg
There build system is based on Automake.


# Using automake:

` $ ./autogen.sh ; make `
