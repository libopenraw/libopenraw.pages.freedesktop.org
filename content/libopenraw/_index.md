+++
title = "libopenraw"
+++

libopenraw is an ongoing project to provide a free software implementation for camera RAW files decoding. One of the main reason is that [dcraw](http://www.cybercom.net/~dcoffin/dcraw/) is not suited for easy integration into applications, and there is a need for an easy to use API to build free software digital image processing application.

It also has the goal to address missing feature from [dcraw](http://www.cybercom.net/~dcoffin/dcraw/) like meta-data decoding and easy thumbnail extraction.

## Documentation

You can view the latest release [API documentation](/api/libopenraw/stable).

## Status

**Alpha release:** Read about [the road to libopenraw
0.4.0](/blog/the-road-to-libopenraw-0-4-0).

Download the latest alpha release 0.4.0.alpha.4
[tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.4.0-alpha.4.tar.bz2)
[GPG
sig](https://libopenraw.freedesktop.org/download/libopenraw-0.4.0-alpha.4.tar.bz2.asc) -
[tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.4.0-alpha.4.tar.xz)
[GPG
sig](https://libopenraw.freedesktop.org/download/libopenraw-0.4.0-alpha.4.tar.xz.asc) -
1 June 2024

Please note the API is unstable and subject to change. Do not use in
released packages.

**Current stable** 0.3.x series

* version 0.3.7 [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.3.7.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.3.7.tar.bz2.asc) - [tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.3.7.tar.xz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.3.7.tar.xz.asc) - 31 July 2023

0.2.x series

* version 0.2.3 [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.bz2.asc) - [tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.xz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.xz.asc) - 9 August 2020

0.1.x series

* version 0.1.3 [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.1.3.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.1.3.tar.bz2.asc) - 3 May 2018

0.0.x series

* version 0.0.9 [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.0.9.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.9.tar.bz2.asc) - 13 Nov 2011
* version 0.0.8 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.8.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.8.tar.gz.asc) - 15 May 2009
* version 0.0.7 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.7.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.7.tar.gz.asc) - 28 Apr 2009
* version 0.0.6 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.6.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.6.tar.gz.asc) - 21 Feb 2009
* version 0.0.5 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.5.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.5.tar.gz.asc) - 26 Feb 2008
* version 0.0.4 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.4.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.4.tar.gz.asc) - 12 Jan 2008
* version 0.0.3 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.3.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.3.tar.gz.asc) - 25 Nov 2007
* version 0.0.2 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.2.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.2.tar.gz.sig) - 25 Jan 2007
* version 0.0.1 [tar.gz](https://libopenraw.freedesktop.org/download/libopenraw-0.0.1.tar.gz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.0.1.tar.gz.sig) - 9 Dec 2006


### Rust bindings

There is the [`libopenraw` crate](https://crates.io/crates/libopenraw)
to use libopenraw from Rust with a Rust API. It is currently limited
to thumbnailing.

## Where to go

* [PlannedFeatures]: what is the plan
* [Getting the code](/getting-the-code): downloading the source
* [Building](/building): building the library
* [Library Documentation](https://libopenraw.freedesktop.org/doxygen) (Doxygen generated)
* [File Formats](formats): our documentation on file formats
* [Links](links): links to external documents that can be helpful

## Getting in touch


### Mailing lists

There is mailing list for developers available now. Visit <https://lists.freedesktop.org/mailman/listinfo/libopenraw-dev> to subscribe. You can also access the archive: <https://lists.freedesktop.org/archives/libopenraw-dev/>


### IRC

Join us on `#libopenraw` on the FreeNode irc network (`irc.freenode.net`)
