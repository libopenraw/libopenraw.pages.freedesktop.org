+++
title = "Misc links"
+++

# Links

This is a bunch of links to external documentation. (currently unchecked)

# Implementation

* [dcraw](http://www.cybercom.net/~dcoffin/dcraw/) the current
  reference implementation for RAW decoding.
* [jrawio](http://www.tidalwave.it/infoglueDeliverLive/ViewPage.action?siteNodeId=180&languageId=1&contentId=206)
  a Java library that implements RAW image decoding
* [exifprobe](https://web.archive.org/web/20110921060119/http://www.virtual-cafe.com/~dhh/tools.d/exifprobe.d/exifprobe.html) (via Internet Archive) ([exifprobe new home on github](https://github.com/hfiguiere/exifprobe/))
  and [Exiv2](http://www.exiv2.org/) that decode several raw files

# Processing

* [dcraw
  tutorial](http://www.guillermoluijk.com/tutorial/dcraw/index_en.htm)
  explain of dcraw works: a basic insight on RAW processing.
* [Debayering a CR2 image with
  openCV](https://stackoverflow.com/questions/40196936/debayering-a-canon-cr2-image-with-opencv)
  more question asked than answered but it give some overview of the
  process.
* Developing a RAW photo by hand [part
  1](http://www.odelama.com/photo/Developing-a-RAW-Photo-by-hand/) and
  [part
  2](http://www.odelama.com/photo/Developing-a-RAW-Photo-by-hand/Developing-a-RAW-Photo-by-hand_Part-2/) - an exploration into the pipeline and how to work it.
* [Pixel Grouping](https://sites.google.com/site/chklin/demosaic/)
* [What does an unprocessed RAW file look like?](https://photo.stackexchange.com/questions/105271/what-does-an-unprocessed-raw-file-look-like)
  ([Petapixel mirror](https://petapixel.com/2019/07/15/what-does-an-unprocessed-raw-file-look-like/)) -
  explanation of RAW files.

## Colour handling

* [RGB/XYZ
  matrixes](http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html)
  the applied mathematics to transform colours
* [Chromatic
  adaptation](http://www.brucelindbloom.com/index.html?Eqn_ChromAdapt.html)
  converting colourspaces using linear algebra, a direct application
  of the above.

# File format

* [RAW file
  standards](http://www.rags-int-inc.com/PhotoTechStuff/RawStandards/)
  try to match current standard with currently undocumented file
  formats and its
  [summary](http://www.rags-int-inc.com/PhotoTechStuff/RawStandards/RawSummary.html).
* [TIFF](http://partners.adobe.com/public/developer/tiff/index.html)
  on which DNG and apparently other RAW format are based on.
* [Adobe DNG](https://helpx.adobe.com/photoshop/digital-negative.html)
  a "standard" format that Adobe tries to push.
