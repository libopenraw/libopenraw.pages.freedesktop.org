+++
title = "Exempi"
aliases = [
    "/wiki/Exempi"
]
+++

Exempi is an implementation of XMP. Version 2.x is based on Adobe XMP
SDK and released under a BSD-style license like Adobe's. The 3 clause
BSD (aka New-BSD) license is compatible with GPL. The licensing is
kept identical in order to allow two-way merging of the code (ie
sending our change to the XMP SDK to Adobe for integration).

The API is C based and means to be used from any language and be
easier to maintain ABI stability.

The overall changes include:

* STABLE C API/ABI for the library, instead of a C++ template based version.
* Rust bindings (available separately).
* No CLA to contribute.
* Build system based on automake.
* Code changes for more UNIX systems compatibility.
* Broader architecture support.
* WebP support.
* Test suite.

There is also a work in progress "exempi" command line tool starting in 2.2.0

# Download Exempi

## 2.x series

* version 2.6.5 [.tar.xz](/download/exempi-2.6.5.tar.xz) - [GPG signature](/download/exempi-2.6.5.tar.xz.asc) - [.tar.bz2](/download/exempi-2.6.5.tar.bz2) - [GPG signature](/download/exempi-2.6.5.tar.bz2.asc)
* version 2.5.2 [.tar.bz2](/download/exempi-2.5.2.tar.bz2) - [GPG signature](/download/exempi-2.5.2.tar.bz2.asc)
* version 2.4.5 [.tar.bz2](/download/exempi-2.4.5.tar.bz2) - [GPG signature](/download/exempi-2.4.5.tar.bz2.asc)
* version 2.3.0 [.tar.bz2](/download/exempi-2.3.0.tar.bz2) - [GPG signature](/download/exempi-2.3.0.tar.bz2.asc)
* version 2.2.2 [.tar.bz2](/download/exempi-2.2.2.tar.bz2) - [GPG signature](/download/exempi-2.2.2.tar.bz2.asc)
* [older releases](/download/)

[Changelog](https://gitlab.freedesktop.org/libopenraw/exempi/blob/master/NEWS)

## Rust crate

To use Exempi 2.4.0 (or later) from Rust, juste use the crates:
[exempi2](https://crates.io/crates/exempi2) and
[exempi-sys](https://crates.io/crates/exempi-sys)

Just specify in your `Cargo.toml` file the following:

    [dependencies]
    exempi2 = "0.1.3"

Use the appropriate version as you see fit. See the
[documentation](https://docs.rs/exempi2/latest/exempi2/).

Note: the crate `exempi2` replace the crate `exempi`. It is mostly API
compatible.

## Binary packages

* check your favorite distribution.

## Obsolete 1.0 series

Before the XMP SDK was available, Exempi started as a new implementation.

* version 0.5.1 [.tar.gz](/download/archived/exempi-0.5.1.tar.gz) -
  [GPG signature](/download/archived/exempi-0.5.1.tar.gz.asc)
* version 0.5.0 [.tar.gz](/download/archived/exempi-0.5.0.tar.gz) -
  [GPG signature](/download/archived/exempi-0.5.0.tar.gz.sig)

# Bugs reporting

Bugs can be reported in the Gitlab issue tracker: [Report a
bug](https://gitlab.freedesktop.org/libopenraw/exempi/issues)

# Mailing list

There is a mailing list exempi-devel to discuss of exempi development
and exempi use (as a developer).
[https://lists.freedesktop.org/mailman/listinfo/exempi-devel](https://lists.freedesktop.org/mailman/listinfo/exempi-devel)

# Source Code repository

You can access everything from the [Gitlab
project](https://gitlab.freedesktop.org/libopenraw/exempi)

# Links

* [CC XMP page](https://wiki.creativecommons.org/XMP)
* [Adobe release XMP sdk under BSD
  license](https://web.archive.org/web/20170120082205/http://blogs.adobe.com/gunar/2007/05/xmp_411_sdk_available_under_bsd_license.html)
  (via the Internet Archive)
