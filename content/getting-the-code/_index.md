The source code is managed by git

# On Freedesktop.org

## Anonymous git

* Check out the tree:
  `git clone https://gitlab.freedesktop.org/libopenraw/libopenraw.git`

## For developers (needs authorization)

Please see the project page
   https://gitlab.freedesktop.org/libopenraw/libopenraw

# Github

There is a full mirror on github at https://github.com/hfiguiere/libopenraw

