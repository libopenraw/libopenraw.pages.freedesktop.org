libopenraw is a desktop agnostic effort to support digital camera RAW
files. It include libopenraw and exempi as the main components.

* [libopenraw](libopenraw), a library to parse digital camera RAW
  files. Also separately the Rust bindings.
* [Exempi](exempi), a library to parse XMP metadata. Also separately
  the Rust bindings.
* [Raw Thumbnailer](raw-thumbnailer), a thumbnailer for camera RAW
  files.
