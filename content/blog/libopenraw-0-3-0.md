+++
date = 2020-12-18
title = "libopenraw 0.3.0 is out"
summary = "[libopenraw](/libopenraw/) 0.3.0 is out."
+++

[libopenraw](/libopenraw/) 0.3.0 is out.

The key new feature of libopenraw 0.3.0 is the metadata extraction
from the various formats, including non TIFF based Canon CR3.

Added a bunch more cameras, new an old, and even older.

Here is the list of changes:

#### New features:

Camera support:

- Added Pentax K-7 (DNG), K5 II (DNG), star-ist DL2*, K110D*, K5 II*, K3*,
  K3 II*, K-S1*.
- Added Leica Monochrom M Typ-246 (DNG), D-LUX 4*, Q2-Monochrom (DNG) and
  SL2-S (DNG).
- Added XIAOYI YDXJ-2 and YIAC-3 (DNG).
- Added Nikon Z5*, Z6 2* and Z7 2*.
- Added Sony A7SIII*, A7C*.
- Added Canon PowerShot S30*, S40*, S45*, S50*, S60*, S70*, S100V*, Pro70,
  EOS 850 D / Rebel T8i / KISS X10i*.
- Added Hasselblad L1D-20c (DNG).
- Added Panasonic S5*.
- Added Zeiss ZX1 (DNG).
- Added Fujifilm FinePix S100FS*, X-S10*.

- Epson R-D1, R-D1s: extract the 640 pixel preview.
- Panasonic: extract the Exif thumbnail, get bit per channel, CFA pattern.
- Olympus, Nikon, Pentax: extract the MakerNote preview.
- Pentax: Extract the active area and white level when available.
- Sony: files properly identified. A390 treated as a A380.
- Canon CRW: synthesize Exif metadata.

#### Changes:

- API: Added `or_ifd_release()`, `or_ifd_get_name()` and `or_ifd_get_type()`.
- API: The `ORIfdDirRef` returned by `or_rawfile_get_ifd()` must
  be released.
- API: `or_rawfile_get_ifd()` support more IFD types.
- API: `or_ifd_index` is replaced by `or_ifd_dir_type`. `OR_IFD_MAKERNOTE` is replaced
  by `OR_IFD_MNOTE`, `OR_IFD_CFA` is replaced by `OR_IFD_RAW`.
- API: `or_iterator*()` to iterate through the metadata tags.
- API: Added `or_metavalue_get_count()`.
- Fix getting MakerNotes for Pentax, Panasonic.
- Get the Panasonic Exif thumbnail.
- Added tool `exifdump`.

#### Internal Changes:

- Use `std::make_unique<>`: requires C++14 to compile.
- Added `LOGASSERT()`.
- Use model ID when available (Sony, Pentax) instead of strings.
- Testsuite list the tests that are not run.
- You can run any testsuite.

#### Bug fixes:

- Panasonic: fix the active area, the bpc and the CFA pattern type.

See `NEWS` included in the tarball.

Download:

* [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.3.0.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.3.0.tar.bz2.asc)
* [tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.3.0.tar.xz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.3.0.tar.xz.asc)
