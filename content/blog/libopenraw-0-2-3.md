+++
date = 2020-08-09
title = "libopenraw 0.2.3 is out"
summary = "[libopenraw](/libopenraw/) 0.2.3 is out."
+++

[libopenraw](/libopenraw/) 0.2.3 is out.

Here are the changes:

Camera support:

- Added Olympus E-M10MarkIV*.
- Added Hasselblad Lunar* (rebadged Sony NEX-7).
- Added iPhone 8, SE, XS.

- Coefficients for Panasonic DC-G100, Sony ZV-1, Canon EOS R5 and EOS R6.

Bug fixes:

- Fix preview for CR3 files.
- Properly recognize Apple as make.
- Fix an overflow in the ljpeg decompressor.

Internal Changes:

- Added SR2 test case in the testsuite.

See `NEWS` included in the tarball.

Download:

* [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.bz2.asc)
* [tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.xz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.3.tar.xz.asc)
