+++
date = 2023-12-24
title = "Exempi 2.6.5 is out"
summary = "[Exempi](/exempi/) 2.6.5 is out."
+++

[Exempi](/exempi/) 2.6.5 is out.

Here are the changes:

- Update XMPCore to Adobe XMP SDK v2023.12
  - Bug fixes
  - C++17 compliance changes
  - Changes to remove deprecated APIs
  - Removing unimplemented APIs in XMPUtils header
- Fix a potential nullptr dereference.
