+++
date = 2023-02-13
title = "libopenraw 0.3.4 is out"
summary = "[libopenraw](/libopenraw/) 0.3.4 is out."
+++

[libopenraw](/libopenraw/) 0.3.4 is out.

Mainly a bug fix related to concurrency.

## New features:

### Camera support:
  [ A * denote that static WB coefficients have been included, unless DNG ]

  - Added Canon R8 and R50.
  - Added Panasonic G95D* and S5M2.

## Bug fixes:

  - Ensure that RawFile::init() is never called twice. This would
    cause crashes if it was called concurrently.

## Internal changes:

  - Change mutability of access to the factory and the extensions.
