+++
date = 2020-07-24
title = "libopenraw 0.2.2 is out"
summary = "[libopenraw](/libopenraw/) 0.2.2 is out."
+++

[libopenraw](/libopenraw/) 0.2.2 is out.

Here are the changes:

New features:

- ordiag: Added a developer mode with `-D`.

Camera support:

- Support for Canon EOS R5 and R6.
- Support for Sony DSC-R1* (SR2).
- Support for Nikon D6*.
- Support for Panasonic DC-G100.
- Support for Leica M10-R.
- Support for GoPro HERO5, HERO6, HERO7 and HERO8 (GPR files, DNG based).
- Added Sony SR2 file support.

Bug fixes:

- Properly detect Canon Digital Rebel (aka 300D).
- Better detect Minolta and Ricoh vendors.
- Fix a crash in DNG on invalid files.

See `RELEASE_NOTES` and `NEWS` included in the tarball.

Download:

* [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.2.2.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.2.tar.bz2.asc)
* [tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.2.2.tar.xz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.2.tar.xz.asc)
