+++
date = 2024-01-07
title = "The road to libopenraw 0.4.0"
summary = "I just released libopenraw 0.4.0-alpha.2. Actually .0 and .1 got tagged but never released."
+++

## 0.4.0-alpha.2

I just released libopenraw 0.4.0-alpha.2. Actually .0 and .1 got
tagged but never released.

**Note: the ABI is subject to change.**

There is a [version on
crates.io](https://crates.io/crates/libopenraw/0.4.0-alpha.2) and the
tarballs in the usual
place. [libopenraw-0.4.0-alpha.2.tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.4.0-alpha.2.tar.xz)
This will build the Rust version of libopenraw to be linked
dynamically. The crate works like any other crate. If you were using
the libopenraw crate before, short of API changes, it should work.

What's left to be done:

- A few bit of C API are still missing
- The colour correction needs completing. This is [issue
  16](https://gitlab.freedesktop.org/libopenraw/libopenraw/-/issues/16).

These are just for the goals of 0.4.0.

I hope to have it out in a couple of month.

## Changes

Here are the changes since 0.3.7:

### New features:

  - The library is now written in Rust. This should provide a safer
    code. Some API changed, see below.
  - Extract White Balance coefficients.

  Camera support:

  [ a * denote that static WB coefficients have been included, unless DNG ]

  - Fujifilm Z-S20*.
  - Nikon Z8*.
  - Pannsonic S5M2X*, ZS200D* (Alias of ZS200).
  - Pentax K3 Mark III DNG is properly identified. Added K3 Mark III
    Monochrome.
  - Sony ILCE-6700*, Z-V1M2*.

### Bug fixes:

  - The white and black values for the RAW data are now correctly set.
    - DNG, ERF, PEF: directly from the files.
    - CR2: from the built-in matrices.
    - Values are now quads (for 4 component patterns)
  - The active area from Olympus ORF is extracted from the files.
  - Fujifilm RAF: get black levels and set white levels and non X-trans CFA.
  - DNG: decompress lossless JPEG tiled images (most of DNG converted files).

### API changes:

  - `or_rawfile_get_rawdata()` now returns a newly allocated raw data.
  - `or_rawfile_get_thumbnail()` now returns a newly allocated thumbnail.
  - `or_rawfile_get_thumbnail_sizes()` may return nullptr on error.
  - `or_rawfile_get_rendered_image()` now returns a new allocated bitmap data.
  - `or_rawfile_get_metavalue()` now take a string key, and the
    MetaValue must be freed.
  - `or_rawfile_get_ifd()` return an owned object.
  - `or_rawfile_get_calibration_illuminantX()` returns a uint32_t
    instead of an enum.
  - `or_metadata_iterator_get_entry()` now just return the entry.
  - Added `or_metadata_iterator_get_dir()` to return the IFD.
  - Added `or_metadata_get_key()`, `or_metadata_get_value()` and
    `or_metadata_get_type()`.
  - Removed `or_bitmapdata_new()`, `or_rawdata_new()`,
    `or_thumbnail_new()` that are no longer needed.
  - Removed `or_ifd_release()` that is no longer needed.
  - `or_metavalue_get_string()` no longer take an index.

### Internal changes

  - The `mp4parse` crate as been moved to `src/mp4/parse`, and
    `mp4parse_capi` has been removed. This allow publishing the crate.

