+++
date = 2020-06-24
title = "Exempi 2.5.2 is out"
+++

[Exempi](/exempi/) 2.5.2 is out.

Here are the changes:

- [Bug #18:](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/18)
  Remove an unnecessary message to stdout in the exempi tool.
- [Bug #20:](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/20)
  Test file formats. This also change the GIF sample to GIF89a.
- [Bug #17:](https://gitlab.freedesktop.org/libopenraw/exempi/issues/17)
  `make check` should build more reliably now.
- [MR !1:](https://gitlab.freedesktop.org/libopenraw/exempi/merge_requests/1)
  Remove deprecated C++ to compile with C++17.
- [MR !2:](https://gitlab.freedesktop.org/libopenraw/exempi/merge_requests/2)
  Fix a build failure with C++17.
