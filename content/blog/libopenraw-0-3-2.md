+++
date = 2022-06-24
title = "libopenraw 0.3.2 is out"
summary = "[libopenraw](/libopenraw/) 0.3.2 is out."
+++

[libopenraw](/libopenraw/) 0.3.2 is out.

Added a bunch more cameras, new an old, and even older.

## New features:

### Camera support:
  [ a * denote that static WB coefficients have been included, unless DNG ]

  - Added Epson RD-1X*.
  - Added Leica DIGILUX3*.
  - Added Nikon D1H*, D7500*, D850* and P7800*.
  - Added Olympus E30*, E420*, E450*, E520*, E600* and E-P5*.
  - Added Pentax K2000* and K-m* (PEF).

## Bug fixes:

  - Properly detect compressed data for Panasonic.
  - Fix the linkage of the mp4parse library with libtool.
  - Fixed the demo/ccfa to output properly the byte stream.
  - Fixed BitIterator code to peek past the number of bits
    for Olympus decoding.
  - Fixed decompression of packed Olympus ORF files.
  - Fixed over reported size of Panasonic compressed Raw data.

## Internal changes:

  - Refactored the Olympus decompressor: less code.
  - Update Mozilla mp4parser to the latest 0.12.0 (rebased) for
    the CR3 parser.
