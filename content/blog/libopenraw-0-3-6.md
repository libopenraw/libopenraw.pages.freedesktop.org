+++
date = 2023-06-10
title = "libopenraw 0.3.6 is out"
summary = "[libopenraw](/libopenraw/) 0.3.6 is out."
+++

[libopenraw](/libopenraw/) 0.3.6 is out.

## Camera support:

[ a * denote that static WB coefficients have been included, unless DNG ]

  - Added Leica M11 Monochrom, Q3.

## Bug fixes:

  - Properly decode uncompressed big endian 16 bit raw data from
    Pentax PEF. Issue #2.  Including PENTAX *ist DS.
    [#2](https://gitlab.freedesktop.org/libopenraw/libopenraw/-/issues/2)
  - Added missing MPL-2.0 license to tarball.
    [#15](https://gitlab.freedesktop.org/libopenraw/libopenraw/-/issues/15)
  - Fix build issue with Rust 1.70.
  - Fix offline Rust build from tarball.
