+++
date = 2024-09-21
title = "Raw thumbnailer 47"
summary = "I just released Raw Thumbailer 47."
+++

I just released Raw Thumbailer 47. Why 47? Because it's GNOME version.

It's totally rewritten in Rust, still using libopenraw.

The best way to use it is to have it installed as part of the distribution.

[Download from GNOME](https://download.gnome.org/sources/raw-thumbnailer/47/)
