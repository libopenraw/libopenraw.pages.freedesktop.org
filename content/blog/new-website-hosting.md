+++
date = "2018-10-28"
title = "New website"
+++

Not exactly new hosting, but Freedesktop.org moved their hosting to a
self hosted Gitlab instance. So we moved. And the website is now
generated using Gitlab pages instead of the Wiki.

[The project Gitlab page](https://gitlab.freedesktop.org/libopenraw)

If you want to contribute to this website, please submit a merge
request for the
[repository](https://gitlab.freedesktop.org/libopenraw/libopenraw.pages.freedesktop.org)

And you can subscribe to the [RSS feed](/index.xml) to get announcements.
