+++
date = 2023-07-31
title = "libopenraw 0.3.7 is out"
summary = "[libopenraw](/libopenraw/) 0.3.7 is out."
+++

[libopenraw](/libopenraw/) 0.3.7 is out.

## Bug fixes:

  - Fix a crash when rendering Leica M Monochron DNG files.
  - Preserve the raw data photometric interpretation when decompressing
    raw data.

## Other:

  - Updated Exif tags.
