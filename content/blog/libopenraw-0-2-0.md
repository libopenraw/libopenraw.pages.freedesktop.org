+++
date = 2020-03-26
title = "libopenraw 0.2.0 is out"
+++

[libopenraw](/libopenraw/) 0.2.0 is out.

Here are the changes:

- Support parsing Canon CR3 and GoPro GPR
- Many many camera added
- New API to get vendor ID

See `RELEASE_NOTES` and `NEWS` included in the tarball.
libopenraw now require Rust to compile the CR3 support.
