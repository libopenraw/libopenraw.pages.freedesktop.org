+++
date = 2020-06-26
title = "libopenraw 0.2.1 is out"
+++

[libopenraw](/libopenraw/) 0.2.1 is out.

Here are the changes:

- Support for Nikon Coolpix P950
- Support for Fujifilm F550EXR
- Support for Sony ZV-1

Internal Changes:

- Updated MP4 parser.

See `RELEASE_NOTES` and `NEWS` included in the tarball.

Download:

* [tar.bz2](https://libopenraw.freedesktop.org/download/libopenraw-0.2.1.tar.bz2) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.1.tar.bz2.asc)
* [tar.xz](https://libopenraw.freedesktop.org/download/libopenraw-0.2.1.tar.xz) [GPG sig](https://libopenraw.freedesktop.org/download/libopenraw-0.2.1.tar.xz.asc)
