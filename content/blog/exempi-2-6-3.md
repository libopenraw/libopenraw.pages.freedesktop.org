+++
date = 2022-12-10
title = "Exempi 2.6.3 is out"
summary = "[Exempi](/exempi/) 2.6.3 is out."
+++

[Exempi](/exempi/) 2.6.3 is out.

Here are the changes:

- [Issue
  #29](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/29):
  Update the config.guess and config.sub files in the tarball.
- Make dist an xz tarball.
- [Issue
  #30](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/30):
  Fix null to int assignement error. (Francisco Ramos)
- [Issue
  #28](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/28):
  Fix build error on armhf. (Fabrice Fontaine)
