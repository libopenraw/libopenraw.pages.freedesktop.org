+++
date = 2022-11-07
title = "libopenraw Rust bindings"
summary = "The [libopenraw crate](https://crates.io/crates/libopenraw) is out."
+++

The libopenraw Rust API is now available.

Just add the following to your `cargo.toml`:
```toml
libopenraw = "0.1.1"
```

Its API is currently limited to thumbnailing. More is to come later.

Note that with the Rust rewrite of libopenraw, this crate will also
change drastically.
