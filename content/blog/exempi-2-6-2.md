+++
date = 2022-06-25
title = "Exempi 2.6.2 is out"
summary = "[Exempi](/exempi/) 2.6.2 is out."
+++

[Exempi](/exempi/) 2.6.2 is out.

Here are the changes:

- Update XMPCore to Adobe XMP SDK v2022.06
  - Build system fixes that don't matter.
- Fix a typo in an ID3 genre string
  [!4](https://gitlab.freedesktop.org/libopenraw/exempi/-/merge_requests/4)
- Build XMP_SDK with some fatal warnings:
  - `-Werror=missing-field-initializers`
  - `-Werror=misleading-indentation`
  - `-Werror=class-memaccess`
- Remove (deprecated) `boost_test_minimal`
  [#24](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/24)
- CI: require "bionic" on Travis CI.
- Remove old `.sym` file (Daniel Kolesa)
  [#17](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/17)
- Added `--enable-samples` (default = yes) (Fabrice Fontaine)
  [!6](https://gitlab.freedesktop.org/libopenraw/exempi/-/merge_requests/6)
