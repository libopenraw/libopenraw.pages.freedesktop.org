+++
date = 2022-02-13
title = "Exempi 2.6.1 is out"
+++

[Exempi](/exempi/) 2.6.1 is out.

Here are the changes:

- [Bug
  #26](https://gitlab.freedesktop.org/libopenraw/exempi/-/issues/26):
  Addressed missing README.md and others from tarball.
