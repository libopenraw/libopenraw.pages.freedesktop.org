+++
date = 2023-04-28
title = "libopenraw 0.3.5 is out"
summary = "[libopenraw](/libopenraw/) 0.3.5 is out."
+++

[libopenraw](/libopenraw/) 0.3.5 is out.

## New features:

  - The Rust crates are vendored for the tarball. Note: resulting
    tarball is bigger.

### Camera support:

[ a * denote that static WB coefficients have been included, unless DNG ]

  - Added coefficients for Canon R8* and R50*.
  - Added coefficients for Panasonic S5M2*.
  - Added Panasonic GF8*.
  - Added Sony ZV-E1*, ILME-FX30*.

## Bug fixes:

  - Fix a missing include with gcc 13. Issue
    [#12](https://gitlab.freedesktop.org/libopenraw/libopenraw/-/issues/12).
