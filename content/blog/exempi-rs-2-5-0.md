+++
date = 2019-09-28
title = "exempi-rs 2.5.0 is out"
summary = "[exempi-rs](/exempi/) 2.5.0 is out. exempi-rs are the Rust binding for Exempi 2.4."
+++

[exempi-rs](/exempi/) 2.5.0 is out. exempi-rs are the Rust binding for Exempi 2.4.

They have been uploaded to [crates.io](https://crates.io) to be available with cargo.

Here are the changes:

- `DateTime` now has standard derive Clone and Debug
  - Fixed ffi struct.
  - Now opaque.
- `xmp_iterator_new()` now take a const `Xmp`
- `XmpIterator::next()` outputs a `PropFlags`, not an `IterFlags`
- `Xmp::set_array_item()` was missing.
- `Xmp` is now `Send`.
- `exempi::init()` will run only once.
- Add `exempi::force_init()`.
- A more Rust like API returning `Result<>`.
- Update URL for crate.

