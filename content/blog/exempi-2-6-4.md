+++
date = 2023-07-06
title = "Exempi 2.6.4 is out"
summary = "[Exempi](/exempi/) 2.6.4 is out."
+++

[Exempi](/exempi/) 2.6.4 is out.

Here are the changes:

- Update XMPCore to Adobe XMP SDK v2023.07
  - Security Fixes.
  - Fix Exif Date missing seconds values.
    [XMP-Toolkit-SDK issue](https://github.com/adobe/XMP-Toolkit-SDK/issues/50)
- Fix some portability build issues with dlopen on BSD:
  [MR 7](https://gitlab.freedesktop.org/libopenraw/exempi/-/merge_requests/7)
  (Alyssa Ross)
- Added missing file type constants from the XMP_SDK.
